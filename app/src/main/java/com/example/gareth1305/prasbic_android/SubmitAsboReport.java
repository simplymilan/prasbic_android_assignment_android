package com.example.gareth1305.prasbic_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gareth1305.prasbic_android.entity.Report;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SubmitAsboReport extends HomePageActivity {

    Button submitReportBtn, useCurrentLocationBtn;
    EditText reportAddressLine1, reportCounty, reportCategory, reporteeEmail;
    Report report;
    EditText reportAddressLine1EditTxt, reportCountyEditTxt, reporteeEmailEditTxt;
    GPSHelper gps;

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkFieldsForEmptyValues();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_asbo_report);

        SharedPreferences preferences = getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE);
        String email = preferences.getString("EMAIL", "");
        reporteeEmailEditTxt = (EditText) findViewById(R.id.reporteeEmailEditTxt);
        reporteeEmailEditTxt.setText(email);

        // Check to make sure the fields are not empty
        reportAddressLine1 = (EditText) findViewById(R.id.reportAddressLine1EditTxt);
        reportCounty = (EditText) findViewById(R.id.reportCountyEditTxt);
        reportCategory = (EditText) findViewById(R.id.reportCategoryEditTxt);
        reporteeEmail = (EditText) findViewById(R.id.reporteeEmailEditTxt);

        reportAddressLine1.addTextChangedListener(textWatcher);
        reportCounty.addTextChangedListener(textWatcher);
        reportCategory.addTextChangedListener(textWatcher);
        reporteeEmail.addTextChangedListener(textWatcher);
        checkFieldsForEmptyValues();

        submitReportBtn = (Button) findViewById(R.id.submitReportBtn);
        submitReportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reportAddressLine1Str = reportAddressLine1.getText().toString();
                String reportCountyStr = reportCounty.getText().toString();
                String reportCategoryStr = reportCategory.getText().toString();
                String reporteeEmailStr = reporteeEmail.getText().toString();

                String fullAddress = reportAddressLine1Str + ", " + reportCountyStr;
                convertAddressToLatLng(fullAddress, reportCategoryStr, reporteeEmailStr);
            }
        });

        useCurrentLocationBtn = (Button) findViewById(R.id.useCurrentLocationBtn);
        useCurrentLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gps = new GPSHelper(getApplicationContext());
                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();
                convertLatLngToAddress(latitude, longitude);
            }
        });
    }



    private void checkFieldsForEmptyValues() {
        submitReportBtn = (Button) findViewById(R.id.submitReportBtn);
        String addLine1 = reportAddressLine1.getText().toString();
        String county = reportCounty.getText().toString();
        String category = reportCategory.getText().toString();
        String reportee = reporteeEmail.getText().toString();

        if (addLine1.length() > 0 && county.length() > 0 &&
                category.length() > 0 && reportee.length() > 0) {
            submitReportBtn.setEnabled(true);
        } else {
            submitReportBtn.setEnabled(false);
        }
    }

    private void convertLatLngToAddress(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String locality = addresses.get(0).getLocality();
            reportAddressLine1EditTxt = (EditText) findViewById(R.id.reportAddressLine1EditTxt);
            reportAddressLine1EditTxt.setText(address + ", " + city);
            reportCountyEditTxt = (EditText) findViewById(R.id.reportCountyEditTxt);
            reportCountyEditTxt.setText(state);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void convertAddressToLatLng(String fullAddress, String category, String email) {
        Geocoder myCoder = new Geocoder(getApplicationContext());
        List<Address> address;
        try {
            address = myCoder.getFromLocationName(fullAddress, 5);
            if (address == null) {
                Log.d("In if", "Address is null");
            }
            Address location = address.get(0);
            String lat = "" + location.getLatitude();
            String lng = "" + location.getLongitude();

            report = new Report(location.getLatitude(), location.getLongitude(), category, email);
            new SubmitReport().execute(lat, lng, category, email);
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), "Error with address", Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    private class SubmitReport extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            BufferedReader inBuffer = null;
            String result = "Success";
            String url = "http://192.168.43.163:8080/create_report";

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost request = new HttpPost(url);
                List<NameValuePair> postParameters =
                        new ArrayList<NameValuePair>();
                postParameters.add(new BasicNameValuePair("reportLatitude", params[0]));
                postParameters.add(new BasicNameValuePair("reportLongitude", params[1]));
                postParameters.add(new BasicNameValuePair("reportCategory", params[2]));
                postParameters.add(new BasicNameValuePair("reporteeUserEmail", params[3]));
                UrlEncodedFormEntity formEntity =
                        new UrlEncodedFormEntity(postParameters);

                request.setEntity(formEntity);
                HttpResponse httpResponse = httpClient.execute(request);
                inBuffer = new BufferedReader(new InputStreamReader(
                        httpResponse.getEntity().getContent()));

                StringBuffer stringBuffer = new StringBuffer("");
                String line = "";
                String newLine = System.getProperty("line.separator");
                while ((line = inBuffer.readLine()) != null) {
                    stringBuffer.append(line + newLine);
                }
                inBuffer.close();
                result = stringBuffer.toString();
            } catch (Exception e) {
                result = e.getMessage();
            } finally {
                if (inBuffer != null) {
                    try {
                        inBuffer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return result;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Intent intent = new Intent(SubmitAsboReport.this, ViewReportsActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
