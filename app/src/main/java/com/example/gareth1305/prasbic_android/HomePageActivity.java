package com.example.gareth1305.prasbic_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.gareth1305.prasbic_android.entity.User;

public class HomePageActivity extends AppCompatActivity {

    Button viewReportsBtn, logMeOutBtn,
            emergencyBtn, submitAsboReportBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        Log.d("CLASS_NAME", this.getClass().getSimpleName());
        viewReportsBtn = (Button) findViewById(R.id.viewReportsBtn);
        logMeOutBtn = (Button) findViewById(R.id.logoutBtn);
        emergencyBtn = (Button) findViewById(R.id.emergencyBtn);
        submitAsboReportBtn = (Button) findViewById(R.id.submitAsboReportBtn);

        viewReportsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewReports();
            }
        });

        logMeOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logMeOut();
            }
        });

        emergencyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emergencyButton();
            }
        });

        submitAsboReportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitAsboReport();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        switch (this.getClass().getSimpleName()) {
            case "HomePageActivity":
                menu.getItem(0).setVisible(false);
                return true;
            case "ViewReportsActivity":
                menu.getItem(2).setVisible(false);
                return true;
            case "SubmitAsboReport":
                menu.getItem(3).setVisible(false);
                return true;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.homePageBtn:
                returnHome();
                return true;
            case R.id.emergencyActionBarBtn:
                emergencyButton();
                return true;
            case R.id.viewReportsActionBarBtn:
                viewReports();
                return true;
            case R.id.createReportActionBarBtn:
                submitAsboReport();
                return true;
            case R.id.logMeOutActionBarBtn:
                logMeOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logMeOut() {
        SharedPreferences.Editor editor =
                getSharedPreferences("LOGIN_INFO", Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
        Intent intent = new Intent(HomePageActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void returnHome() {
        Intent intent = new Intent(HomePageActivity.this, HomePageActivity.class);
        startActivity(intent);
        finish();
    }

    private void viewReports() {
        Intent intent = new Intent(HomePageActivity.this, ViewReportsActivity.class);
        startActivity(intent);
    }

    private void emergencyButton() {
        Intent intent = new Intent(HomePageActivity.this, EmergencyButtonActivity.class);
        startActivity(intent);
    }

    private void submitAsboReport() {
        Intent intent = new Intent(HomePageActivity.this, SubmitAsboReport.class);
        startActivity(intent);
    }
}
