package com.example.gareth1305.prasbic_android.entity;

import java.io.Serializable;

/**
 * Created by Gareth1305 on 21/11/2015.
 */
public class Report implements Serializable {

    private int id;
    private double reportLatitude;
    private double reportLongitude;
    private String reportCategory;
    private String reporteeUserEmail;

    public Report(int id, double reportLatitude,
                  double reportLongitude, String reportCategory,
                  String reporteeUserEmail) {
        this.id = id;
        this.reportLatitude = reportLatitude;
        this.reportLongitude = reportLongitude;
        this.reportCategory = reportCategory;
        this.reporteeUserEmail = reporteeUserEmail;
    }

    public Report(double reportLatitude,
                  double reportLongitude, String reportCategory,
                  String reporteeUserEmail) {
        this.reportLatitude = reportLatitude;
        this.reportLongitude = reportLongitude;
        this.reportCategory = reportCategory;
        this.reporteeUserEmail = reporteeUserEmail;
    }

    public Report() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getReportLatitude() {
        return reportLatitude;
    }

    public void setReportLatitude(double reportLatitude) {
        this.reportLatitude = reportLatitude;
    }

    public double getReportLongitude() {
        return reportLongitude;
    }

    public void setReportLongitude(double reportLongitude) {
        this.reportLongitude = reportLongitude;
    }

    public String getReportCategory() {
        return reportCategory;
    }

    public void setReportCategory(String reportCategory) {
        this.reportCategory = reportCategory;
    }

    public String getReporteeUserEmail() {
        return reporteeUserEmail;
    }

    public void setReporteeUserEmail(String reporteeUserEmail) {
        this.reporteeUserEmail = reporteeUserEmail;
    }

    @Override
    public String toString() {
        return "Report{" +
                "id=" + id +
                ", reportLatitude=" + reportLatitude +
                ", reportLongitude=" + reportLongitude +
                ", reportCategory='" + reportCategory + '\'' +
                ", reporteeUserEmail='" + reporteeUserEmail + '\'' +
                '}';
    }
}
